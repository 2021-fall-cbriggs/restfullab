package com.oreillyauto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RestfullabApplication {

	public static void main(String[] args) {
		SpringApplication.run(RestfullabApplication.class, args);
	}

}
